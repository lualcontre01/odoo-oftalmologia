# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Enterprise Management Solution
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from openerp import models, fields, api


class EstudiosOftalmologicos(models.Model):
    _name = 'estudios.oftalmologicos'
    _rec_name = 'paciente_id'
    _description = 'Carga de estudios oftalmologicos'

    fecha = fields.Date('Fecha')
    expediente_id = fields.Many2one('expediente.clinico', 'Expediente')
    paciente_id = fields.Many2one('registro.paciente', readonly='True',
                                  compute='_compute_id')
    nom_paciente = fields.Char('Nombre del paciente', readonly='True',
                               compute='_compute_name')
    rel_sexo = fields.Selection(string='Sexo', selection=[('f', 'Femenino'),
                                                          ('m', 'Masculino')],
                                related='paciente_id.sexo', readonly='True')
    rel_edad = fields.Integer(string='Edad', related='paciente_id.edad',
                              readonly='True')
    fecha_estudio = fields.Date('Fecha Estudio')
    topografia_corneal = fields.Boolean('Topografia Corneal')
    paquimetria = fields.Boolean('Paquimetria')
    micros_especular = fields.Boolean('Microscopia Especular')
    oct_paq_nervio = fields.Boolean('OCT Paquete Nervio Optico + Macula')
    oct_macula = fields.Boolean('OCT Macula')
    campimetria = fields.Boolean('Campimetria')
    ultra_ecografia = fields.Boolean('Ultrasonido/Ecografia')
    fot_fondo_oido = fields.Boolean('Fotografias de Fondo de Ojo')
    fot_nervio_optico = fields.Boolean('Fotografia Nervio Optico')
    fluoran = fields.Boolean('Fluorangiografia')
    cal_lente = fields.Boolean('Calculo LIO 1 ojo')
    oct_nervio_optico = fields.Boolean('OCT Nervio Óptico')
    calc_lio_ao = fields.Boolean('Calculo LIO Ambos Ojos')
    otros = fields.Char('Otros')

    def print_estudios(self, data):
        return self.env.ref('med_oftalmologia.report_estudios_oftalmologicos')\
            .report_action([], data={})

    @api.multi
    @api.depends('expediente_id')
    def _compute_name(self):
        for record in self:
            record.nom_paciente = record.expediente_id.paciente_id.name

    @api.multi
    @api.depends('expediente_id')
    def _compute_id(self):
        for record in self:
            record.paciente_id = record.expediente_id.paciente_id.id
