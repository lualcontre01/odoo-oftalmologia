# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Enterprise Management Solution
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from openerp import models, fields, api


class NotaEvolutiva(models.Model):
    _name = 'nota.evolutiva'
    _rec_name = 'name'
    _description = 'New Description'

    paciente_id = fields.Many2one('registro.paciente', 'Paciente')
    fecha = fields.Date('Fecha')
    edad = fields.Integer('Edad')
    rel_sexo = fields.Selection(string='Sexo', selection=[('f', 'Femenino'),
                                                          ('m', 'Masculino')])
    rel_edad = fields.Integer(string='Edad', related='paciente_id.edad')
    numero_expediente = fields.Char('Numero Expediente')
    evolucion = fields.Text('Evolucion y Actualizacion del cuadro Clinico')
    res_estudios = fields.Text('Resultados Relevantes de Estudios')
    diag_problemas = fields.Text('Diagnostico o Problemas Criticos')
    medicantento_ids = fields.Many2one('oftalmologia.medicamento',
                                       'oftalmologia_id', 'Medicamento')
    nombre_oftalmologo = fields.Char('Nombre Oftalmologo')
    ced_ofalmologo = fields.Char('Cedula Oftalmologo')


class NotaEstudio(models.Model):
    _name = 'nota.estudio'
    _description = 'Nota de Estudio'

    paciente_id = fields.Many2one('registro.paciente','Paciente')
    rel_sexo = fields.Selection(string='Sexo', selection=[('f', 'Femenino'),
                                                          ('m', 'Masculino')])
    rel_edad = fields.Integer(string='Edad', related='paciente_id.edad')
    fecha = fields.Date('Fecha')
    nro_expediente = fields.Char('Numero de Expendiente')
    tipo_estudio = fields.Char('Tipo de Estudio Realizado')
    observaciones = fields.Text('Observaciones')
    adjuntos = fields.Binary('Adjuntos')
