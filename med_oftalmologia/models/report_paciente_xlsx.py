from odoo import models
import logging

_logger = logging.getLogger(__name__)

workbook_format = {
            'bold': 1,
            'border': 1,
            'align': 'center',
            'valign': 'vcenter',
            'fg_color': 'white'
}


class RiskManagementXlsx(models.AbstractModel):
    _name = 'report.med_oftalmologia.oftalmologia_xlsx'
    _inherit = 'report.report_xlsx.abstract'

    def generate_xlsx_report(self, workbook, data, ofta):
        merge_format = workbook.add_format(workbook_format)
        report_name = ofta.rel_nombre_paciente
        sheet = workbook.add_worksheet(report_name[:31])
        row = 0
        col = 0
        #Cabeceras del documento
        sheet.write_string(row, col, 'Numero de Expediente')
        sheet.write_string(row, col + 1, 'Nombre del paciente')
        sheet.write_string(row, col + 2, 'Edad')
        sheet.write_string(row, col + 3, 'Sexo')
        sheet.write_string(row, col + 4, 'Telefono 1')
        sheet.write_string(row, col + 5, 'Telefono 2')
        sheet.write_string(row, col + 6, 'Email')
        sheet.write_string(row, col + 7, 'Fecha de nacimiento')
        sheet.write_string(row, col + 8, 'Referencia')
        sheet.write_string(row, col + 9, 'Fecha de diagnostico')
        sheet.write_string(row, col + 10, 'Diagnostico')
        sheet.write_string(row, col + 11, 'Plan')
        sheet.write_string(row, col + 12, 'Fecha Ultima nota evolutiva')
        sheet.write_string(row, col + 13, 'Diagnostico ultima nota evolutiva')

        row = 1
        col = 0
        citas = self.env['agenda.citas'].search([('paciente_id', '=', ofta.id)])
        ventas = self.env['sale.order'].search([('partner_id.name', '=', ofta.paciente_id.name),
                                                ('state', '=', 'sale')])
        for record in ofta:
            sheet.write_string(row, col, record.numero_expediente or '')
            sheet.write_string(row, col + 1, record.rel_nombre_paciente or '')
            sheet.write_string(row, col + 2, record.rel_edad or '')
            sheet.write_string(row, col + 3, record.rel_sexo or '')
            sheet.write_string(row, col + 4, record.paciente_id.telefono1 or '')
            sheet.write_string(row, col + 5, record.paciente_id.telefono2 or '')
            sheet.write_string(row, col + 6, record.paciente_id.mail or '')
            sheet.write_string(row, col + 7, record.paciente_id.fecha_nacimiento or '')
            sheet.write_string(row, col + 12, record.nota_evolutiva_ids[-1].fecha or '')
            sheet.write_string(row, col + 13, record.nota_evolutiva_ids[-1].diag_problemas or '')
        for cita in citas:
            sheet.write_string(row, col + 8, cita.referencia_id.refiere or '')
            row += 1
        row = 1
        for oftalmologia in ofta.oftalmologia_ids:
            sheet.write_string(row, col + 9, oftalmologia.fecha or '')
            sheet.write_string(row, col + 10, oftalmologia.diagnostico or '')
            sheet.write_string(row, col + 11, oftalmologia.plan or '')
            row += 1

        row = 0
        col = 0
        count = 1
        for venta in ventas:
            header_factura = 'Fecha de factura {}'.format(count)
            sheet.write_string(row, col + 14, header_factura or '')
            sheet.write_string(row + 1, col + 14, venta.confirmation_date or '')
            col += 1
            count += 1
        row = 0
        col = 0
        count = 1
        for venta in ventas:
            for line in venta:
                header_concepto = 'Concepto de factura {}'.format(count)
                sheet.write_string(row, col + 15, header_concepto or '')
                sheet.write_string(row + 1, col + 15, venta.confirmation_date or '')
                col += 1
                count += 1



