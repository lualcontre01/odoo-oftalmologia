# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Enterprise Management Solution
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from openerp import models, fields, api


class Ofalmologia(models.Model):
    _name = 'oftalmologia'
    _description = 'Oftalmologia'

    od_orbita_anexos = fields.Char('Ojo Derecho Orbita y Anexos',
                                   default='Sin Alteraciones')
    od_conjuntiva = fields.Char('Ojo Derecho Conjuntiva',
                                default='Sin Alteraciones')
    od_cornea = fields.Char('Ojo Derecho Cornea',
                            default='Sin Alteraciones')
    od_iris = fields.Char('Ojo Derecho Iris',
                          default='Sin Alteraciones')
    od_cristalino = fields.Char('Ojo Derecho Cristalino')
    oi_orbita_anexos = fields.Char('Ojo Izquierdo Orbita y Anexos',
                                   default='Sin Alteraciones')
    oi_conjuntiva = fields.Char('Ojo Izquierdo Conjuntiva',
                                default='Sin Alteraciones')
    oi_cornea = fields.Char('Ojo Izquierdo Cornea',
                            default='Sin Alteraciones')
    oi_iris = fields.Char('Ojo Izquierod Iris',
                          default='Sin Alteraciones')
    oi_cristalino = fields.Char('Ojo Izquierdo Cristalino')
    diagnostico = fields.Text('Diagnostico')
    medicantento_ids = fields.Many2one('oftalmologia.medicamento',
                                       'oftalmologia_id', 'Medicamento')
    otra = fields.Char('Otra')
    plan = fields.Text('Plan')
    resumen_clinico = fields.Text('Resumen Clinico')
    seguimiento = fields.Text('Seguimiento')
    nombre_oftalmologo = fields.Char('Nombre Oftalmologo')
    ced_oftalmologo = fields.Char('Cedula Oftalmologo')
    

class OftalmologiaMedicamento(models.Model):
    _name = 'oftalmologia.medicamento'
    _description = 'Medicamentos Prescritos'

    dosis = fields.Char('Dosis')
    presentacion = fields.Char('Presentacion')
    via_administracion = fields.Char('Via Administracion')
    frecuencia = fields.Char('Frecuencia')
    duracion = fields.Char('Duracion')

