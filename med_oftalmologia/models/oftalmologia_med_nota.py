# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Enterprise Management Solution
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from openerp import models, fields, api


class Ofalmologia(models.Model):
    _name = 'oftalmologia'
    _description = 'Registro de Evaluacion Oftalmologica'

    expediente_id = fields.Many2one('expediente.clinico')
    fecha = fields.Date('Fecha')
    od_orbita_anexos = fields.Char('Ojo Derecho Orbita y Anexos',
                                   default='Sin Alteraciones')
    oi_orbita_anexos = fields.Char('Ojo Izquierdo Orbita y Anexos',
                                   default='Sin Alteraciones')
    od_conjuntiva = fields.Char('Ojo Derecho Conjuntiva',
                                default='Sin Alteraciones')
    oi_conjuntiva = fields.Char('Ojo Izquierdo Conjuntiva',
                                default='Sin Alteraciones')
    od_cornea = fields.Char('Ojo Derecho Cornea',
                            default='Sin Alteraciones')
    oi_cornea = fields.Char('Ojo Izquierdo Cornea',
                            default='Sin Alteraciones')
    od_iris = fields.Char('Ojo Derecho Iris',
                          default='Sin Alteraciones')
    oi_iris = fields.Char('Ojo Izquierod Iris',
                          default='Sin Alteraciones')
    od_cristalino = fields.Char('Ojo Derecho Cristalino',
                                default='Sin Alteraciones')
    oi_cristalino = fields.Char('Ojo Izquierdo Cristalino',
                                default='Sin Alteraciones')
    av_od = fields.Char('Agudeza Visual Ojo Derecho')
    av_oi = fields.Char('Agudeza Visual Ojo Izquierdo')
    diagnostico = fields.Text('Diagnostico')
    medicantento_ids = fields.One2many('medicamento.oftalmologia',
                                       'oftalmologia_id', 'Medicamento')
    otra = fields.Char('Otra')
    plan = fields.Text('Plan')
    resumen_clinico = fields.Text('Resumen Clinico')
    seguimiento = fields.Text('Seguimiento')
    nombre_oftalmologo_id = fields.Many2one('registro.doctores', 'Oftalmologo')
    ced_oftalmologo = fields.Char('Cedula Oftalmologo',
                                  related='nombre_oftalmologo_id.cedula')

    od_retina = fields.Char('Ojo Derecho Retina', default='Sin Alteraciones')
    od_nervio_optico = fields.Char('Ojo Derecho Nervio Optico',
                                   default='Sin Alteraciones')
    oi_nervio_optico = fields.Char('Ojo Izquierdo Nervio Optico',
                                   default='Sin Alteraciones')
    oi_retina = fields.Char('Ojo Izquierdo Retina', default='Sin Alteraciones')
    tension_intra = fields.Char('Tension Intraocular',
                                default='Sin Alteraciones')


class NotaEvolutiva(models.Model):
    _name = 'nota.evolutiva'
    _description = 'Registro Nota Evolutiva'

    fecha = fields.Date('Fecha')
    expediente_id = fields.Many2one('expediente.clinico')
    paciente_id = fields.Many2one('registro.paciente', 'Paciente')
    fecha = fields.Date('Fecha')
    rel_sexo = fields.Selection(string='Sexo', selection=[('f', 'Femenino'),
                                                          ('m', 'Masculino')]
                                , related='paciente_id.sexo')
    rel_edad = fields.Integer(string='Edad', related='paciente_id.edad')
    numero_expediente = fields.Char('Numero Expediente')
    evolucion = fields.Text('Evolucion y Actualizacion del cuadro Clinico')
    res_estudios = fields.Text('Resultados Relevantes de Estudios')
    diag_problemas = fields.Text('Diagnostico o Problemas Criticos')

    medicantento_ids = fields.One2many('medicamento.evolutivo',
                                       'nota_evolutiva_id', 'Medicamento')
    otro = fields.Char('Otro')
    nombre_oftalmologo_id = fields.Many2one('registro.doctores', 'Oftalmologo')
    ced_oftalmologo = fields.Char('Cedula Oftalmologo')


class NotaEstudio(models.Model):
    _name = 'nota.estudio'
    _description = 'Nota de Estudio'

    fecha = fields.Date('Fecha')
    expediente_id = fields.Many2one('expediente.clinico')
    paciente_id = fields.Many2one('registro.paciente', 'Paciente')
    rel_sexo = fields.Selection(string='Sexo', selection=[('f', 'Femenino'),
                                                          ('m', 'Masculino')]
                                , related='paciente_id.sexo')
    rel_edad = fields.Integer(string='Edad', related='paciente_id.edad')
    fecha = fields.Date('Fecha')
    nro_expediente = fields.Char('Numero de Expendiente')
    tipo_estudio = fields.Char('Tipo de Estudio Realizado')
    observaciones = fields.Text('Observaciones')
    archivos_ids = fields.One2many('archivos.adjuntos', 'nota_id',
                                   'Archivos Adjuntos')


class ArchivosAdjuntos(models.Model):
    _name = 'archivos.adjuntos'
    _rec_name = 'nombre_archivo'
    _description = 'Carga de archivos adjuntos de nota de estudio'

    nota_id = fields.Many2one('nota.estudio')
    nombre_archivo = fields.Char('Nombre del Archivo')
    adjunto = fields.Binary('Adjunto')


class MedicamentoEvolutivo(models.Model):
    _name = 'medicamento.evolutivo'
    _description = 'Medicamentos Prescritos'

    nota_evolutiva_id = fields.Many2one('nota.evolutiva')
    nombre_comercial = fields.Char('Nombre Comercial')
    ing_activo = fields.Char('Ing Activo')
    dosis = fields.Char(u'Dósis', default="1 Gota")
    presentacion = fields.Char('Presentacion', default="Gotas")
    via_administracion = fields.Char('Via Administracion', default=u"Tópica")
    frecuencia = fields.Char('Frecuencia', default="Cada X horas")
    duracion = fields.Char('Duracion', default=u"X días")


class MedcamentoOfalmologico(models.Model):
    _name = 'medicamento.oftalmologia'
    _description = 'Medicamentos Prescritos'

    oftalmologia_id = fields.Many2one('oftalmologia')
    nombre_comercial = fields.Char('Nombre Comercial')
    ing_activo = fields.Char('Ing Activo')
    dosis = fields.Char(u'Dósis', default="1 Gota")
    presentacion = fields.Char('Presentacion', default="Gotas")
    via_administracion = fields.Char('Via Administracion', default=u"Tópica")
    frecuencia = fields.Char('Frecuencia', default="Cada X horas")
    duracion = fields.Char('Duracion', default=u"X días")
