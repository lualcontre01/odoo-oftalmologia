# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Enterprise Management Solution
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from openerp import models, fields, api


class VentaLentes(models.Model):
    _name = 'venta.lentes'
    _description = 'Venta de Lentes'
    _rec_name = 'marca'

    fecha = fields.Date('Fecha')
    expediente_id = fields.Many2one('expediente.clinico')
    material = fields.Selection(string="Material",
                                selection=[('poli', 'Poli'),
                                           ('cr', 'CR39'),
                                           ('h', 'H-IDX'),
                                           ('cris', 'Cristal')])
    dip = fields.Char('Dip')
    alt_obj = fields.Char('ATL. OBJ')
    antireflejo = fields.Selection(string="Antirreflejo",
                                   selection=[('Si', 'Si'),
                                              ('No', 'No')])
    marca = fields.Char('Marca')
    fotocrom = fields.Selection(string="Fotocromatico",
                                selection=[('Si', 'Si'),
                                           ('No', 'No')])
    monofocal = fields.Boolean('Monofocal')
    bifocal = fields.Boolean('Bifocal')
    prog = fields.Boolean('Prog')
    otro = fields.Char('Otro')
